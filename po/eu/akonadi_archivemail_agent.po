# Translation for akonadi_archivemail_agent.po to Euskara/Basque (eu).
# Copyright (C) 2021-2024 This file is copyright:
# This file is distributed under the same license as the kmail package.
# SPDX-FileCopyrightText: 2023, 2024 KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2021, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: kmail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-16 00:39+0000\n"
"PO-Revision-Date: 2024-05-19 09:08+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.02.2\n"

#: addarchivemaildialog.cpp:21
#, kde-format
msgctxt "@title:window"
msgid "Modify Archive Mail"
msgstr "Aldatu posta artxiboa"

#: addarchivemaildialog.cpp:23 archivemailwidget.cpp:279
#, kde-format
msgctxt "@title:window"
msgid "Add Archive Mail"
msgstr "Gehitu posta artxiboa"

#: addarchivemailwidget.cpp:28
#, kde-format
msgid "Archive all subfolders"
msgstr "Artxibatu azpikarpeta guztiak"

#: addarchivemailwidget.cpp:38
#, kde-format
msgid "Folder:"
msgstr "Karpeta:"

#: addarchivemailwidget.cpp:48
#, kde-format
msgid "Format:"
msgstr "Formatua:"

#: addarchivemailwidget.cpp:56
#, kde-format
msgid "Path:"
msgstr "Bide-izena:"

#: addarchivemailwidget.cpp:63
#, kde-format
msgid "Backup each:"
msgstr "Babes-kopia hainbestero:"

#: addarchivemailwidget.cpp:75
#, kde-format
msgid "Maximum number of archive:"
msgstr "Artxiboen gehienezko kopurua:"

#: addarchivemailwidget.cpp:78
#, kde-format
msgid "unlimited"
msgstr "mugagabea"

#: archivemailinfo.cpp:101 archivemailinfo.cpp:117
#, kde-format
msgctxt "Start of the filename for a mail archive file"
msgid "Archive"
msgstr "Artxiboa"

#: archivemailrangewidget.cpp:22
#, kde-format
msgid "Use Range"
msgstr "Erabili barrutia"

#: archivemailwidget.cpp:72
#, kde-format
msgid "Name"
msgstr "Izena"

#: archivemailwidget.cpp:72
#, kde-format
msgid "Last archive"
msgstr "Azken artxiboa"

#: archivemailwidget.cpp:72
#, kde-format
msgid "Next archive in"
msgstr "Hurrengo artxiboa hemen,"

#: archivemailwidget.cpp:72
#, kde-format
msgid "Storage directory"
msgstr "Biltegiratzeko direktorioa"

#: archivemailwidget.cpp:91
#, kde-format
msgid "Archive Mail Agent"
msgstr "Posta artxibatzeko agentea"

#: archivemailwidget.cpp:93
#, kde-format
msgid "Archive emails automatically."
msgstr "Artxibatu e-postak automatikoki."

#: archivemailwidget.cpp:95
#, kde-format
msgid "Copyright (C) 2014-%1 Laurent Montel"
msgstr "Copyright (C) 2013-%1 Laurent Montel"

#: archivemailwidget.cpp:96
#, kde-format
msgctxt "@info:credit"
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: archivemailwidget.cpp:96
#, kde-format
msgid "Maintainer"
msgstr "Arduraduna"

#: archivemailwidget.cpp:98
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Iñigo Salvador Azurmendi"

#: archivemailwidget.cpp:98
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xalba@ni.eus"

#. i18n: ectx: property (text), widget (QPushButton, addItem)
#: archivemailwidget.cpp:109 ui/archivemailwidget.ui:31
#, kde-format
msgid "Add..."
msgstr "Gehitu..."

#: archivemailwidget.cpp:115
#, kde-format
msgid "Open Containing Folder..."
msgstr "Ireki hau barnean duen karpeta..."

#. i18n: ectx: property (text), widget (QPushButton, deleteItem)
#: archivemailwidget.cpp:118 ui/archivemailwidget.ui:45
#, kde-format
msgid "Delete"
msgstr "Ezabatu"

#: archivemailwidget.cpp:169
#, kde-format
msgid "Folder: %1"
msgstr "Karpeta: %1"

#: archivemailwidget.cpp:191
#, kde-format
msgid "Tomorrow"
msgid_plural "%1 days"
msgstr[0] "Bihar"
msgstr[1] "%1 egun"

#: archivemailwidget.cpp:200
#, kde-format
msgid "Archive will be done %1"
msgstr "Artxiboa egiteko %1"

#: archivemailwidget.cpp:236
#, kde-format
msgid "Do you want to delete the selected items?"
msgstr "Hautatutako elementuak ezabatu nahi dituzu?"

#: archivemailwidget.cpp:237
#, kde-format
msgctxt "@title:window"
msgid "Delete Items"
msgstr "Ezabatu elementuak"

#: archivemailwidget.cpp:278
#, kde-format
msgid ""
"Cannot add a second archive for this folder. Modify the existing one instead."
msgstr ""
"Ezin du karpeta honetarako bigarren artxibo bat gehitu. Horren ordez, "
"dagoena aldatu."

#: job/archivejob.cpp:58
#, kde-format
msgid "Directory does not exist. Please verify settings. Archive postponed."
msgstr ""
"Direktorioa ez da existitzen. Mesedez, egiaztatu ezarpenak. Artxibatzea "
"geroratu da."

#: job/archivejob.cpp:75
#, kde-format
msgid "Start to archive %1"
msgstr "Hasi %1 artxibatzen"

#. i18n: ectx: property (text), widget (QPushButton, modifyItem)
#: ui/archivemailwidget.ui:38
#, kde-format
msgid "Modify..."
msgstr "Aldatu..."

#: widgets/formatcombobox.cpp:14
#, kde-format
msgid "Compressed Zip Archive (.zip)"
msgstr "Konprimitutako Zip artxiboa (.zip)"

#: widgets/formatcombobox.cpp:15
#, kde-format
msgid "Uncompressed Archive (.tar)"
msgstr "Konprimitu gabeko artxiboa (.tar)"

#: widgets/formatcombobox.cpp:16
#, kde-format
msgid "BZ2-Compressed Tar Archive (.tar.bz2)"
msgstr "BZ2-rekin konprimitutako Tar artxiboa (.tar.bz2)"

#: widgets/formatcombobox.cpp:17
#, kde-format
msgid "GZ-Compressed Tar Archive (.tar.gz)"
msgstr "GZ-rekin konprimitutako Tar artxiboa (.tar.gz)"

#: widgets/unitcombobox.cpp:13
#, kde-format
msgid "Days"
msgstr "Egun"

#: widgets/unitcombobox.cpp:14
#, kde-format
msgid "Weeks"
msgstr "Aste"

#: widgets/unitcombobox.cpp:15
#, kde-format
msgid "Months"
msgstr "Hilabete"

#: widgets/unitcombobox.cpp:16
#, kde-format
msgid "Years"
msgstr "Urte"

#~ msgid "Add Archive Mail"
#~ msgstr "Gehitu posta artxiboa"

#~ msgid "Remove items"
#~ msgstr "Kendu elementuak"

#~ msgid "Remove"
#~ msgstr "Kendu"
